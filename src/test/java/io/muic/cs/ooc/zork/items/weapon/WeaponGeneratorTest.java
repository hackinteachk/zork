package io.muic.cs.ooc.zork.items.weapon;

import static org.junit.Assert.*;

public class WeaponGeneratorTest {

    @org.junit.Test
    public void generate() throws InstantiationException, IllegalAccessException {
        Weapon sword = WeaponGenerator.generate("sword");
        System.out.println(sword);
        assertEquals("sword",sword.getName());
    }
}