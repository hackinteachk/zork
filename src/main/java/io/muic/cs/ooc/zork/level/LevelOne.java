package io.muic.cs.ooc.zork.level;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.items.*;
import io.muic.cs.ooc.zork.items.weapon.Sword;
import io.muic.cs.ooc.zork.items.weapon.Weapon;
import io.muic.cs.ooc.zork.map.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static io.muic.cs.ooc.zork.Utils.*;
import static io.muic.cs.ooc.zork.Game.player;

public class LevelOne implements Level {

    private Room start;
    private final int MAX_POTION = 40;
    private Set<Room> rooms;
    private final Map<String, Integer> MONS_MAX = new HashMap<String, Integer>(){
        {
            put("MAX_HP",50);
            put("MAX_ATP",20);
        }
    };

    public LevelOne() {
        rooms = createRoom();
    }

    @Override
    public boolean complete() {
        for(Room r : rooms){
            if(r.hasMonster()){
                return false;
            }
        }
        formatPrompt("Level 1 complete !");
        return true;
    }

    private void setStartRoom(Room r) {
        this.start = r;
    }


    private Set<Room> createRoom() {
        /* Declaration of rooms */
        Room bedroom, kitchen,soccerField;

        bedroom = new Room.Builder("bedroom")
                .addItem(new Potion(MAX_POTION))
                .setDescription("Starter room")
                .build();

        setStartRoom(bedroom);

        kitchen = new Room.Builder("kitchen")
                .addItem(new Potion(MAX_POTION))
                .addItem(new Shield())
                .addItem(new Sword())
                .addMonster(new Monster(MONS_MAX,"Godzilla"))
                .setDescription("some potion and food to eat!")
                .build();

        soccerField = new Room.Builder("soccer field")
                .addItem(new Sword())
                .setDescription("let's play football")
                .addMonster(new Monster(MONS_MAX,"Grasshopper"))
                .build();

        bedroom.addNearby("north",kitchen);
        bedroom.addNearby("west",soccerField);

        kitchen.addNearby("east",bedroom);

        soccerField.addNearby("west",bedroom);


        Set<Room> rooms = new HashSet<>();
        rooms.add(bedroom);
        rooms.add(kitchen);
        rooms.add(soccerField);

        return rooms;
    }

    public void start() {
        formatPrompt("---- Level 1 ----");
        player.setCurrentRoom(start);
    }
}
