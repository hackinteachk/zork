package io.muic.cs.ooc.zork.commands;

import java.util.HashMap;

import static io.muic.cs.ooc.zork.Utils.*;

public final class CommandFactory {

    private static final HashMap<String, Command> commands = new HashMap<String, Command>() {
        {
            put("north", new Walk("north"));
            put("south", new Walk("south"));
            put("east", new Walk("east"));
            put("west", new Walk("west"));

            put("take", new Take());
            put("info", new Info());
            put("exit", new Quit());
            put("quit", new Quit());
            put("help", new Help());
            put("drop", new Drop());
            put("look", new Look());
            put("use" , new Use());
        }
    };

    public static Command getCommand(String name) {
        return commands.get(name);
    }

    public static boolean containsCommand(String name) {
        if (!commands.containsKey(name)) {
            formatPrompt("Command not found ! use 'help' to see available command");
            return false;
        }
        return true;
    }

}
