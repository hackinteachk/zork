package io.muic.cs.ooc.zork.commands;

import static io.muic.cs.ooc.zork.Game.*;

public class Walk implements Command{

    private String direction;

    public Walk(String direction){
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "walking";
    }

    @Override
    public void act() {
        player.walk(direction);
    }
}
