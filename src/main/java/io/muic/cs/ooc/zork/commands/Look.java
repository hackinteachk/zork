package io.muic.cs.ooc.zork.commands;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.items.Item;

import static io.muic.cs.ooc.zork.Game.player;
import static io.muic.cs.ooc.zork.Utils.formatPrompt;

public class Look implements Command {

    @Override
    public void act() {

        formatPrompt("You are in " + player.currentRoom().toString());
        formatPrompt(player.currentRoom().getDescription());
        formatPrompt("This room contains :");
        if (player.currentRoom().getAllItems().isEmpty()) {
            formatPrompt("nothing");
        } else {
            for (Item i : player.currentRoom().getAllItems()) {
                formatPrompt(i.getName());
            }
        }
        if (player.getFacingMonster() != null) {
            formatPrompt("you found monster!");
            for (Monster m : player.currentRoom().getMonsters()) {
                formatPrompt(m.getName());
            }
        }

    }
}
