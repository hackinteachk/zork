package io.muic.cs.ooc.zork.commands;

import static io.muic.cs.ooc.zork.Utils.*;

public class Quit implements Command{

    @Override
    public String toString() {
        return "quit";
    }

    @Override
    public void act() {
        formatConfirmation("quit");
        String confirmation = in.nextLine();
        if(confirmExit(confirmation)){
            formatPrompt("Good bye...");
            System.exit(0);
        }else{
            formatPrompt("Let's resume the game.");
        }
    }


    private boolean confirmExit(String s){
        if(s.compareTo("y") == 0) return true;
        return false;
    }
}
