package io.muic.cs.ooc.zork.items.weapon;

import io.muic.cs.ooc.zork.items.Item;

import static io.muic.cs.ooc.zork.Game.player;

public class Gun extends Weapon {
    private final int DAMAGE = 100;

    public Gun() {
    }

    @Override
    public void use() {
        player.getFacingMonster().hit(DAMAGE);
    }

    @Override
    public String getName() {
        return "gun";
    }

    @Override
    public String usage() {
        return "shoot!";
    }
}
