package io.muic.cs.ooc.zork.commands;

import static io.muic.cs.ooc.zork.Game.*;

public class Info implements Command {
    @Override
    public String toString() {
        return "info";
    }

    public void act() {
        printInfo();
    }

    private void printInfo(){
        //@TODO Print something
        if(player != null){
            player.shout();
        }
    }
}
