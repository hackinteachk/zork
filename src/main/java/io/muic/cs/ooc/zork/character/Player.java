package io.muic.cs.ooc.zork.character;

import io.muic.cs.ooc.zork.Game;
import io.muic.cs.ooc.zork.items.Item;
import io.muic.cs.ooc.zork.items.Shield;
import io.muic.cs.ooc.zork.items.weapon.Weapon;
import io.muic.cs.ooc.zork.map.Room;

import java.util.*;

import static io.muic.cs.ooc.zork.Utils.*;

public class Player {

    private int hp;
    private String username;
    private Room currentRoom;
    private List<Item> inventory = new ArrayList<>();
    private Monster facingMonster;
    private int shieldCount = 0;

    public Player(String username) {
        this.hp = 100;
        this.username = username;
    }

    public void setCurrentRoom(Room room){
        this.currentRoom = room;
    }

    public void shout(){
        System.out.println(username);
        System.out.printf("HP : %d\n",hp);
        System.out.printf("Current room : %s\n",currentRoom.getName());
        System.out.println("Inventory status:");
        for(Item i : inventory){
            System.out.printf("%s ",i.getName());
        }
        System.out.println("");
        System.out.printf("Shield status : ");
        if(shieldIsActive()){
            System.out.println("ON ["+shieldCount+"]");
        }else{
            System.out.println("OFF");
        }
    }

    public boolean useItem(String item){
        //@TODO add criteria for using item
        Item i;
        if((i=searchInventory(item)) != null){
            i.use();
            if(!(i instanceof Weapon)){
                inventory.remove(i);
            }
            return true;
        }
        return false;
    }

    public void hit(int decreasedAmnt){
        if(!shieldIsActive()){
            System.out.println("Player > Ahh, hit by the monster");
            assert(hp >= decreasedAmnt);
            hp -= decreasedAmnt;
            if(hp <= 0){
                Game.over();
            }
        }else{
            useShield();
            if(shieldCount < 0){
                deactivateShield();
            }
        }

    }

    public void healed(int healAmount){
        this.hp += healAmount;
    }

    private Item searchInventory(String item){
        for(Item i : inventory){
            if(item.compareTo(i.getName()) == 0){
                return i;
            }
        }
        return null;
    }

    public boolean drop(String item){
        Item i;
        if((i = searchInventory(item)) != null){
            inventory.remove(i);
            return true;
        }
        return false;
    }

    public int getNumShield(){
        return shieldCount;
    }

    public void activateShield(){
        shieldCount = 3;
    }

    public void useShield(){
        shieldCount--;
        if(shieldCount <= 0){
            deactivateShield();
        }
    }

    public void deactivateShield(){
        shieldCount = -1;
    }

    public boolean shieldIsActive(){
        return shieldCount >= 0;
    }

    public boolean take(String item){
        if(currentRoom.containItem(item)){
            inventory.add(currentRoom.getItems(item));
            Item i = currentRoom.removeItem(item);
            if(i instanceof Shield){
                shieldCount++;
            }
            return true;
        }
        return false;
    }

    public void facingMonster(Monster m){
        this.facingMonster = m;
    }

    public Monster getFacingMonster() {
        return facingMonster;
    }

    public Room currentRoom(){
        return this.currentRoom;
    }

    public void walk(String direction){
        if(currentRoom.validDirection(direction)){
            currentRoom = currentRoom.go(direction);
            formatPrompt("going "+direction);
            formatPrompt("current room : "+currentRoom.toString());
        }
        if(currentRoom().hasMonster()){
            formatPrompt("Monster appeared!");
            formatPrompt("Try to finish them before they can kill you!");
        }
    }

    public String getName() {
        return username;
    }

    @Override
    public String toString() {
//        StringBuilder builder = new StringBuilder();
//        return builder.append(this.username)
//                .append("\n")
//                .append("HP left: ")
//                .append(hp)
//                .append("\n")
//                .append("Attact power: ")
//                .append(attack_power).toString();

        return this.username +
                "\nHP left : " +
                hp;

    }
}
