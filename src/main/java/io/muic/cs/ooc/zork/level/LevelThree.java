package io.muic.cs.ooc.zork.level;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.items.Potion;
import io.muic.cs.ooc.zork.items.Shield;
import io.muic.cs.ooc.zork.items.weapon.Sword;
import io.muic.cs.ooc.zork.map.Room;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static io.muic.cs.ooc.zork.Game.player;
import static io.muic.cs.ooc.zork.Utils.formatPrompt;

public class LevelThree implements Level{

    private Room start;
    private final int MAX_POTION = 80;
    private Set<Room> rooms;
    private final Map<String, Integer> MONS_MAX = new HashMap<String, Integer>(){
        {
            put("MAX_HP",70);
            put("MAX_ATP",30);
        }
    };

    public LevelThree() {
        rooms = createRoom();
    }

    @Override
    public boolean complete() {
        for(Room r : rooms){
            if(r.hasMonster()){
                return false;
            }
        }
        formatPrompt("Level 1 complete !");
        return true;
    }

    private void setStartRoom(Room r) {
        this.start = r;
    }


    private Set<Room> createRoom() {
        /* Declaration of rooms */
        Room comLab, chemLab, phyLab, drawingRoom;

        comLab = new Room.Builder("computer lab")
                .addItem(new Potion(MAX_POTION))
                .addItem(new Shield())
                .addItem(new Sword())
                .setDescription("Refill your inventory!")
                .build();

        chemLab = new Room.Builder("chemistry lab")
                .addMonster(new Monster(MONS_MAX,"MonoSodiumGlutamate"))
                .setDescription("Just fight!")
                .build();

        phyLab = new Room.Builder("physics lab")
                .addMonster(new Monster(MONS_MAX,"Quantum Cat"))
                .addItem(new Shield())
                .setDescription("Quantum is your friend...")
                .build();

        drawingRoom = new Room.Builder("drawing room")
                .addItem(new Potion(MAX_POTION))
                .addItem(new Shield())
                .addMonster(new Monster(MONS_MAX,"Paper"))
                .setDescription("Would you mind draw on paper!")
                .build();


        setStartRoom(comLab);

        comLab.addNearby("north",chemLab);
        comLab.addNearby("west",drawingRoom);
        comLab.addNearby("south",phyLab);

        chemLab.addNearby("west",comLab);

        phyLab.addNearby("south",comLab);
        phyLab.addNearby("west",chemLab);
        phyLab.addNearby("north",drawingRoom);

        drawingRoom.addNearby("west",phyLab);
        drawingRoom.addNearby("south",chemLab);

        Set<Room> rooms = new HashSet<>();
        rooms.add(chemLab);
        rooms.add(comLab);
        rooms.add(drawingRoom);
        rooms.add(phyLab);

        return rooms;
    }

    public void start() {
        formatPrompt("---- Level 3 ----");
        player.setCurrentRoom(start);
    }

}
