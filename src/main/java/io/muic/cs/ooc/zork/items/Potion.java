package io.muic.cs.ooc.zork.items;


import static io.muic.cs.ooc.zork.Game.*;
import static io.muic.cs.ooc.zork.Utils.*;

public class Potion implements Item{

    private Integer amount;

    public Potion(int max_amount) {
        this.amount = random.nextInt(max_amount);
    }

    @Override
    public void use() {
        player.healed(amount);
    }

    @Override
    public String getName() {
        return "potion";
    }

    @Override
    public String usage() {
        return "heal player's hp to some random amount";
    }
}
