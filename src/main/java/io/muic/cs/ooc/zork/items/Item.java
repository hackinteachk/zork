package io.muic.cs.ooc.zork.items;


public interface Item {

    void use();

    @Override
    String toString();

    String getName();

    String usage();
}
