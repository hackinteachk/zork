package io.muic.cs.ooc.zork.commands;

import static io.muic.cs.ooc.zork.Game.player;
import static io.muic.cs.ooc.zork.Utils.formatPrompt;
import static io.muic.cs.ooc.zork.Utils.in;

public class Drop implements Command {

    @Override
    public String toString() {
        return "Dropped";
    }

    @Override
    public void act() {
        System.out.printf("drop > ");
        String toDrop = in.nextLine();
        if(!player.drop(toDrop)){
            formatPrompt("no "+toDrop+" in your inventory!");
        }else{
            formatPrompt("drop "+toDrop);
        }
    }

}
