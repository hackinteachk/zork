package io.muic.cs.ooc.zork.items.weapon;

import java.util.HashMap;

public final class WeaponGenerator {

    private static final HashMap<String, Class<? extends Weapon>>  weapons = new HashMap<String, Class<? extends Weapon>>(){
        {
            put("sword",Sword.class);
        }
    };

    public static Weapon generate(String type) throws IllegalAccessException, InstantiationException {
        if (trueType(type)) {
            return weapons.get(type).newInstance();
        }
        return null;
    }

    private static boolean trueType(String type){
        return weapons.keySet().contains(type);
    }

}
