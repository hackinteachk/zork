package io.muic.cs.ooc.zork.level;

public interface Level {

    boolean complete();

    void start();
}
