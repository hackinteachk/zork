package io.muic.cs.ooc.zork.level;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.items.Potion;
import io.muic.cs.ooc.zork.items.Shield;
import io.muic.cs.ooc.zork.items.weapon.Gun;
import io.muic.cs.ooc.zork.items.weapon.Sword;
import io.muic.cs.ooc.zork.map.Room;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static io.muic.cs.ooc.zork.Game.player;
import static io.muic.cs.ooc.zork.Utils.formatPrompt;

public class LevelTwo implements Level {
    private Room start;
    private final int MAX_POTION = 50;
    private Set<Room> rooms;
    private final Map<String, Integer> MONS_MAX = new HashMap<String, Integer>(){
        {
            put("MAX_HP",70);
            put("MAX_ATP",30);
        }
    };

    public LevelTwo() {
        rooms = createRoom();
    }

    @Override
    public boolean complete() {
        for(Room r : rooms){
            if(r.hasMonster()){
                return false;
            }
        }
        formatPrompt("Level 2 complete !");
        return true;
    }

    private void setStartRoom(Room r) {
        this.start = r;
    }


    private Set<Room> createRoom() {
        Room store, lobby, garage;

        store = new Room.Builder("store")
                .addItem(new Potion(MAX_POTION))
                .addItem(new Sword())
                .addItem(new Shield())
                .addItem(new Shield())
                .addMonster(new Monster(MONS_MAX,"Thief"))
                .setDescription("Grab anything you want")
                .build();

        lobby = new Room.Builder("hotel lobby")
                .addItem(new Gun())
                .setDescription("Nothing interesting in here.")
                .build();

        garage = new Room.Builder("garage")
                .addItem(new Potion(MAX_POTION))
                .addItem(new Shield())
                .build();

        setStartRoom(store);

        store.addNearby("west",lobby);
        store.addNearby("east",garage);

        garage.addNearby("south",store);

        lobby.addNearby("north",store);

        rooms = new HashSet<Room>(){
            {
                add(store);
                add(lobby);
                add(garage);
            }
        };
        return rooms;
    }

    public void start() {
        formatPrompt("---- Level 2 ----");
        player.setCurrentRoom(start);
    }
}
