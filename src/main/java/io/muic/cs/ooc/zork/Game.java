package io.muic.cs.ooc.zork;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.character.Player;
import io.muic.cs.ooc.zork.commands.Command;
import io.muic.cs.ooc.zork.commands.CommandFactory;
import io.muic.cs.ooc.zork.items.weapon.WeaponGenerator;
import io.muic.cs.ooc.zork.level.Level;
import io.muic.cs.ooc.zork.level.LevelOne;
import io.muic.cs.ooc.zork.level.LevelThree;
import io.muic.cs.ooc.zork.level.LevelTwo;
import io.muic.cs.ooc.zork.map.Room;

import java.util.LinkedList;
import java.util.Queue;

import static io.muic.cs.ooc.zork.Utils.formatPrompt;
import static io.muic.cs.ooc.zork.Utils.in;
import static io.muic.cs.ooc.zork.Utils.prompt;

public class Game {


    public static final CommandFactory cmdFact = new CommandFactory();
    public static final WeaponGenerator weaponGen = new WeaponGenerator();
    private static final Queue<Level> levels = new LinkedList<Level>() {{
        add(new LevelOne());
        add(new LevelTwo());
        add(new LevelThree());

    }};
    public static Player player;
    private static Level currentLevel;

    public Game() {

    }


    public void start() {
        String username = getUsername();

        player = new Player(username);

        currentLevel = levels.peek();
        currentLevel.start();

        while (true) {
            if(player.currentRoom().hasMonster()){
                monsterFight();
            }
            getCommand();
//            checkLevelState();
        }

    }

    private void monsterFight(){
        Room currentRoom = player.currentRoom();

        while(player.currentRoom().hasMonster()){

            Monster m = player.currentRoom().sendMonster();
            player.facingMonster(m);

            getCommand();

            if(m.isDead()){
                checkLevelState();
                continue;
            }

            /* Player escape */
            if(!currentRoom.getName().equals(player.currentRoom().getName())){
                currentRoom.takeMonsterBack(m);
                return;
            }

            m.attack();

            /* Monster still alive */
            if(m.isDead()){
                checkLevelState();
                return;
            }
            player.currentRoom().takeMonsterBack(m);

        }
    }

    private String getUsername() {
        String usr;
        do {
            System.out.printf("Enter player name : ");
            usr = in.nextLine();
        } while (!validName(usr));
        return usr;
    }

    private boolean validName(String username) {
        if (username.replace(" ", "").compareTo("") == 0) {
            System.out.println("Invalid name, please try again...");
            return false;
        } else {
            return true;
        }
    }

    private void getCommand() {
        prompt();
        String cmd = in.nextLine();
        cmd.replace(" ","");
        if (cmdFact.containsCommand(cmd)) {
            Command command = cmdFact.getCommand(cmd);
            command.act();
        }
    }

    public static void over() {
        formatPrompt("You're killed by a monster");
        formatPrompt("---- GAME OVER ----");
        System.exit(0);
    }

    private void checkLevelState() {
        if (currentLevel.complete()) {
            levels.poll();
            if(levels.isEmpty()){
                formatPrompt("Congrats, you win !");
                System.exit(0);
            }
            currentLevel = levels.peek();
            currentLevel.start();
        }
    }
}
