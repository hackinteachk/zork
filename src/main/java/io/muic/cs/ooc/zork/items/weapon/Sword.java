package io.muic.cs.ooc.zork.items.weapon;

import static io.muic.cs.ooc.zork.Game.*;
import static io.muic.cs.ooc.zork.Utils.*;

public class Sword extends Weapon {

    private final int DAMAGE = 30;

    public Sword() {
    }

    @Override
    public void use() {
        if(player.getFacingMonster() != null) {
            player.getFacingMonster().hit(DAMAGE);
        }else{
            formatPrompt("There's no monster in this room!");
        }
    }

    @Override
    public String getName() {
        return "sword";
    }

    @Override
    public String usage() {
        return "Attack monster using sword";
    }
}
