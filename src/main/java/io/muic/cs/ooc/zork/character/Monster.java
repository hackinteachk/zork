package io.muic.cs.ooc.zork.character;

import io.muic.cs.ooc.zork.items.Shield;
import io.muic.cs.ooc.zork.map.Room;

import java.util.Map;

import static io.muic.cs.ooc.zork.Game.*;
import static io.muic.cs.ooc.zork.Utils.*;

public class Monster {

    private int hp;
    private int attackPower;
    private Room currentRoom;
    private String name;

    public Monster(Map<String,Integer> MONS_MAX, String name){
        this.hp = MONS_MAX.get("MAX_HP");
        this.attackPower = MONS_MAX.get("MAX_ATP");
        this.name = name;
    }

    public void setCurrentRoom(Room r){
        currentRoom = r;
    }

    public void shout(){
        monsterShout("fighting with "+name);
    }

    /**
     * Is hit by player
     * @param amount amount the player hit monster
     */
    public void hit(int amount){
        monsterShout("GRZZZZZZZZZ");
        hp -= amount;
        isDead();
        monsterShout("HP left: "+hp);
    }


    public void monsterShout(String message) {
        System.out.println(name + " > " + message);
    }

    public boolean isDead(){
        if(hp <= 0){
            player.currentRoom().removeMonster(this.name);
            hp = 0;
            this.currentRoom = null;
            player.facingMonster(null);
            monsterShout("you killed me!");
            return true;
        }
        return false;
    }

    public void attack(){
        player.hit(attackPower);

    }

    public String getName(){
        return name;
    }

    public Room currentRoom(){
        return this.currentRoom;
    }
}
