package io.muic.cs.ooc.zork.commands;

public interface Command {

    @Override
    String toString();

    void act();
}
