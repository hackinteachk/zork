package io.muic.cs.ooc.zork.items.weapon;

import io.muic.cs.ooc.zork.items.Item;

public abstract class Weapon implements Item {


    public Weapon(){
    }

    @Override
    abstract public void use();


    @Override
    abstract public String getName();

    @Override
    abstract public String usage();
}
