package io.muic.cs.ooc.zork.items;

import static io.muic.cs.ooc.zork.Game.*;
import static io.muic.cs.ooc.zork.Utils.*;

public class Shield implements Item {

    @Override
    public void use()
    {
        player.activateShield();
    }

    @Override
    public String toString() {
        return "shield";
    }

    @Override
    public String getName() {
        return "shield";
    }

    @Override
    public String usage() {
        return "protect you from monster's attack";
    }
}
