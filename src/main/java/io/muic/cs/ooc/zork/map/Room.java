package io.muic.cs.ooc.zork.map;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.items.Item;

import java.util.*;

import static io.muic.cs.ooc.zork.Utils.formatPrompt;

public class Room {

    private Map<String, Room> nearby = new HashMap<>();
    private List<Item> items = new ArrayList<>();
    private String name;
    private String description;
    private Stack<Monster> monsters = new Stack<>();

    private Room(String name) {
        this.name = name;
    }

    public void addNearby(String location, Room r){
        nearby.put(location,r);
    }

    public Room go(String direction) {
        return nearby.get(direction);
    }

    public boolean validDirection(String direction) {
        if (!nearby.containsKey(direction)) {
            formatPrompt("No place to go " + direction);
            return false;
        }
        return true;
    }

    public Item removeItem(String item){
        Item i = searchItem(item);
        try {
            if (i != null) {
                items.remove(i);
                return i;
            }
        }catch(NullPointerException e){
            e.printStackTrace();
        }
        return null;
    }

    private Item searchItem(String item){
        for(Item i : items){
            if(item.compareTo(i.getName()) == 0){
                return i;
            }
        }
        return null;
    }

    public boolean hasMonster(){
        return !monsters.isEmpty();
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return name;
    }

    public Item getItems(String item) {
        for (Item i : items) {
            if (item.compareTo(i.getName()) == 0){
                return i;
            }
        }
        return null;
    }

    public List<Item> getAllItems() {
        return items;
    }

    public boolean containItem(String wantToTake) {
        for(Item i : items) {
            if(wantToTake.compareTo(i.getName()) == 0){
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void removeMonsterFamily(List<String> mNames){
        monsters.removeAll(mNames);
    }

    public void removeMonster(String name) {
        for(Monster m : monsters){
            if(m.getName().compareTo(name) == 0){
                monsters.remove(m);
            }
        }
    }

    public Stack<Monster> getMonsters() {
        return monsters;
    }

    public Monster sendMonster() {
        return monsters.pop();
    }

    public void takeMonsterBack(Monster m){
        monsters.push(m);
    }

    public static class Builder{
        private List<Item> items = new ArrayList<>();
        private String name;
        private String description;
        private Stack<Monster> monsters = new Stack<>();

        public Builder(String name){
            this.name = name;
        }

        public Builder addMonster(Monster m){
            this.monsters.push(m);
            return this;
        }

        public Builder addItem(Item i){
            this.items.add(i);
            return this;
        }

        public Builder setDescription(String descr){
            this.description = descr;
            return this;
        }

        public Room build(){
            Room r = new Room(name);

            for(Monster m : monsters){
                m.setCurrentRoom(r);
            }

            r.items = this.items;
            r.monsters = this.monsters;
            r.description = this.description;

            return r;
        }
    }

    //    public static class Builder {
//        private HashMap<String, Room> nearby = new HashMap<>();
//        private String name;
//
//        public Builder(String name) {
//            this.name = name;
//        }
//
//        public Builder west(Room room){
//            nearby.put("west",room);
//            return this;
//        }
//
//        public Builder east(Room room){
//            nearby.put("east",room);
//            return this;
//        }
//
//        public Builder south(Room room){
//            nearby.put("south",room);
//            return this;
//        }
//
//        public Builder north(Room room){
//            nearby.put("north",room);
//            return this;
//        }
//
//        public Room build(){
//            Room room = new Room(name);
//
//            room.nearby = this.nearby;
//            return room;
//        }
//
//    }
}
