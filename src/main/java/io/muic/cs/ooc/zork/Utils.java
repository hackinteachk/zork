package io.muic.cs.ooc.zork;

import io.muic.cs.ooc.zork.character.Monster;
import io.muic.cs.ooc.zork.commands.Command;

import java.util.Random;
import java.util.Scanner;

public class Utils {
    public static Scanner in = new Scanner(System.in);
    public static Random random = new Random();

    public static void formatConfirmation(String confirmation) {
        System.out.printf("ZORK: Are you sure you want to " + confirmation + " ? (y/n)> ");
    }

    public static void formatPrompt(String message) {
        System.out.println("ZORK> " + message);
    }

    public static void prompt() {
        System.out.printf("command > ");
    }
}
