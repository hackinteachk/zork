package io.muic.cs.ooc.zork.commands;

import static io.muic.cs.ooc.zork.Game.player;
import static io.muic.cs.ooc.zork.Utils.formatPrompt;
import static io.muic.cs.ooc.zork.Utils.in;

public class Take implements Command{
    @Override
    public String toString() {
        return "take";
    }

    @Override
    public void act() {
        System.out.printf("take > ");
        String wantToTake = in.nextLine();
        if(!player.take(wantToTake)){
            formatPrompt("no "+wantToTake+" in the room.");
        }else{
            formatPrompt("put "+wantToTake+" into inventory");
        }
    }
}
