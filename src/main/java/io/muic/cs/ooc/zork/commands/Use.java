package io.muic.cs.ooc.zork.commands;

import static io.muic.cs.ooc.zork.Utils.*;
import static io.muic.cs.ooc.zork.Game.*;

public class Use implements Command {

    @Override
    public void act() {
        System.out.printf("use > ");
        String toUse = in.nextLine();
        if(!player.useItem(toUse)){
            formatPrompt("nothing to use.");
        }else{
            formatPrompt("successfully use "+toUse);
        }
    }
}